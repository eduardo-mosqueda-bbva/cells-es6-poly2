// Extend Polymer.Element base class
class CellsEs6Poly2 extends Polymer.Element {

  static get is() {
    return 'cells-es6-poly2';
  }

  static get properties() {
    return {
      something: {
        type: String,
        value: 'I am a shiny Poly2 component'
      }
    };
  }

}

// Register custom element definition using standard platform API
customElements.define(CellsEs6Poly2.is, CellsEs6Poly2);
