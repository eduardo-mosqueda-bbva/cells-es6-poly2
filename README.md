# cells-es6-poly2

Your component description.

Example:
```html
<cells-es6-poly2></cells-es6-poly2>
```

## Styling

The following custom properties and mixins are available for styling:

| Custom property | Description     | Default        |
|:----------------|:----------------|:--------------:|
| --cells-es6-poly2  | Mixin applied to :host     | {}  |
| --cells-fontDefault  | Mixin applied to :host font-family    | sans-serif  |
